package com.wiyatmoko.movies.data.database

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wiyatmoko.movies.data.Result

@Dao
interface MoviesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllMovies(moviesEntity: List<Result>)

    @Query("SELECT * FROM table_movies ORDER BY id DESC")
    fun readMovies(): PagingSource<Int, Result>

    @Query("SELECT * FROM table_movies WHERE id=:id")
    fun getSelectedMovies (id:Int): Result

    @Query("SELECT * FROM table_movies WHERE title LIKE '%' ||:query ||'%' OR overview LIKE '%' ||:query ||'%'")
    fun searchMovies (query:String): List<Result>

    @Query("DELETE FROM table_movies")
    suspend fun clearAll()

}