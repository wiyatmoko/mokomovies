package com.wiyatmoko.movies.data

data class GenreMovie(
    val genres: List<Genre>
)