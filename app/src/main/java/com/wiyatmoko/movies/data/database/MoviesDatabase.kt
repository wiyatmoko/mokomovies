package com.wiyatmoko.movies.data.database

import com.wiyatmoko.movies.data.RemoteKeys


import android.content.Context
import androidx.room.*
import com.wiyatmoko.movies.data.Result


@Database(
    entities = [Result::class, RemoteKeys::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(MoviesTypeConverter::class)
abstract class MoviesDatabase : RoomDatabase() {

    abstract fun moviesDao(): MoviesDao
    abstract fun remoteKeysDao(): RemoteKeysDao

    companion object {
        private var INSTANCE: MoviesDatabase? = null
        fun getInstance(context: Context): MoviesDatabase =
            INSTANCE ?: buildDatabase(context).also {
                INSTANCE = it
            }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            MoviesDatabase::class.java, "movies.db"
        )
            .build()
    }
}