package com.wiyatmoko.movies.data

data class ResultVideo(
    val id: Int,
    val results: List<ResultYoutubeVideo>
)