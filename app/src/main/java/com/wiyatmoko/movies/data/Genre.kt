package com.wiyatmoko.movies.data

data class Genre(
    val id: Int,
    val name: String
)
