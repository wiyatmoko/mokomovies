package com.wiyatmoko.movies.data

data class Dates(
    val maximum: String,
    val minimum: String
)