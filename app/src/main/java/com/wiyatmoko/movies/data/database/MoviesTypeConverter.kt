package com.wiyatmoko.movies.data.database



import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wiyatmoko.movies.data.ResponseMovie
import com.wiyatmoko.movies.data.Result


class MoviesTypeConverter {

    var gson = Gson()

    @TypeConverter
    fun moviesToString(responseMovie: List<Result>): String {
        return gson.toJson(responseMovie)
    }

    @TypeConverter
    fun resultToString(result: com.wiyatmoko.movies.data.Result): String {
        return gson.toJson(result)
    }

    @TypeConverter
    fun stringToResult(data: String): com.wiyatmoko.movies.data.Result {
        val listType = object : TypeToken<Result>() {}.type
        return gson.fromJson(data, listType)
    }


}