package com.wiyatmoko.movies.data.network

import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.data.ResponseDetailMovie
import com.wiyatmoko.movies.data.ResponseMovie
import com.wiyatmoko.movies.data.ResultVideo
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface MovieApiService {

    @GET("/3/movie/now_playing")
    suspend fun getMoviePlaying(
        @Query("page") page: Int,
        @Query("api_key") apiKey: String
    ): ResponseMovie

    @GET("/3/movie/{movieId}")
    suspend fun getMovieDetail(@Path("movieId") movieId: Int, @Query("api_key") apiKey: String
    ): ResponseDetailMovie

    @GET("3/search/movie")
    suspend fun getMovieSearch(
        @Query("query") query: String,
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): ResponseMovie

    @GET("3/movie/{movieId}/videos")
    suspend fun getVideo(@Path("movieId") movieId: Int, @Query("api_key") apiKey: String) : ResultVideo

    @GET("3/genre/movie/list")
    suspend fun getGenre( @Query("api_key") apiKey: String): GenreMovie

    @GET("/3/discover/movie")
    suspend fun getMovieGenre(@Query("api_key") apiKey: String, @Query("with_genres") withGenres: Int) : ResponseMovie

}