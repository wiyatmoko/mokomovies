package com.wiyatmoko.movies.usecase

import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.data.ResponseMovie
import com.wiyatmoko.movies.repository.Repository

class GetMovieGenres ( private val repository: Repository)
{
    suspend fun getMovieGenre (apiKey: String, withGenres: Int) : ResponseMovie = repository.getMovieGenre(apiKey, withGenres)
}