package com.wiyatmoko.movies.usecase

import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.data.ResponseDetailMovie
import com.wiyatmoko.movies.repository.Repository

class MovieDetailOnlineUseCases(
    private val repository: Repository
) {
    suspend fun getMovieDetailOnline(id:Int, apiKey: String): ResponseDetailMovie = repository.getMovieDetailOnline(id,apiKey)


}
