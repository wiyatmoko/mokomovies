package com.wiyatmoko.movies.usecase

import com.wiyatmoko.movies.repository.Repository
import com.wiyatmoko.movies.data.Result
class SearchMovieLocalUseCase(
    private val repository: Repository
) {
    suspend fun searchMoviesLocal(query: String): List<Result> = repository.searchMovies(query)
}