package com.wiyatmoko.movies.usecase

import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.repository.Repository

class GetGenreMovie (
    private val repository: Repository)
    {
        suspend fun getGenre(apiKey: String): GenreMovie =  repository.genreMovie(apiKey)
}