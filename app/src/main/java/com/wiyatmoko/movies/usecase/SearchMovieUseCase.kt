package com.wiyatmoko.movies.usecase

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.wiyatmoko.movies.repository.Repository
import com.wiyatmoko.movies.data.Result
class SearchMovieUseCase(
    private val repository: Repository
){
    fun searchMovies(query: String): LiveData<PagingData<Result>> = repository.getSearch(query)
}
