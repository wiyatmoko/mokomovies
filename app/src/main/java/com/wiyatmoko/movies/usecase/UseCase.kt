package com.wiyatmoko.movies.usecase

data class UseCase(
    val getAllMoviesUseCase: GetAllMoviesUseCase,
    val searchMovieUseCase: SearchMovieUseCase,
    val movieDetailUseCases: MovieDetailUseCases,
    val movieDetailOnlineUseCases: MovieDetailOnlineUseCases,
    val searchMovieLocalUseCase: SearchMovieLocalUseCase,
    val genreMovie: GetGenreMovie,
    val getGenreMovie: GetMovieGenres,
    val getMovieVideo: GetMovieVideo
)
