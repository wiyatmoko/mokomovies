package com.wiyatmoko.movies.usecase

import com.wiyatmoko.movies.data.ResponseMovie
import com.wiyatmoko.movies.data.ResultVideo
import com.wiyatmoko.movies.repository.Repository

class GetMovieVideo ( private val repository: Repository)
{
    suspend fun getMovieVideo (id: Int, apiKey: String) : ResultVideo = repository.getMovieVideo(id, apiKey)
}