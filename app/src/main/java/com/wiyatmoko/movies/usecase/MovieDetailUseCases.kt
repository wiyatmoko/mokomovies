package com.wiyatmoko.movies.usecase

import com.wiyatmoko.movies.repository.Repository
import com.wiyatmoko.movies.data.Result
class MovieDetailUseCases(
    private val repository: Repository
) {
    suspend fun getMovieDetail(id:Int): Result = repository.getMovieDetail(id = id)
}
