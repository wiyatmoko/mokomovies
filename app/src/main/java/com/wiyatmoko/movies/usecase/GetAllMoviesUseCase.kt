package com.wiyatmoko.movies.usecase

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.wiyatmoko.movies.repository.Repository
import kotlinx.coroutines.flow.Flow
import com.wiyatmoko.movies.data.Result

class GetAllMoviesUseCase(
    private val repository: Repository
) {

    fun getPagingNowPlayingMovieFLow(): Flow<PagingData<Result>> = repository.getAllMovies()
    fun getPagingNowPlayingMovieLiveData(): LiveData<PagingData<Result>> = repository.getAllLiveDataMovies()
}
