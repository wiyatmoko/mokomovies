package com.wiyatmoko.movies.repository

import com.wiyatmoko.movies.data.database.MoviesDatabase
import com.wiyatmoko.movies.data.Result
class LocalDataSourceImpl(database: MoviesDatabase):LocalDataSource {

    private val movieDao = database.moviesDao()
    override suspend fun getSelectedMovies(id: Int): Result {
        return movieDao.getSelectedMovies(id= id)
    }

    override suspend fun searchMovies(query: String): List<Result> {
        return movieDao.searchMovies(query = query)
    }
}