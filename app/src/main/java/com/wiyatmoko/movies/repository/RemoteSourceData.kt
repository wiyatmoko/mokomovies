package com.wiyatmoko.movies.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.data.ResponseDetailMovie
import com.wiyatmoko.movies.data.ResponseMovie
import kotlinx.coroutines.flow.Flow
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.data.ResultVideo

interface RemoteSourceData {
    fun getAllMovies(): Flow<PagingData<Result>>
    fun getAllMoviesLiveData(): LiveData<PagingData<Result>>
    fun getSearchMovies(): Flow<PagingData<Result>>
    fun getSearchMoviesLiveData(query: String): LiveData<PagingData<Result>>
    fun getMovieDetail(id: Int): Result
    suspend fun getMovieDetailOnline(id: Int, apiKey: String): ResponseDetailMovie
    suspend fun getGenre(apiKey: String): GenreMovie
    suspend fun getMovieGenre(apiKey: String, withGenres: Int) : ResponseMovie
    suspend fun getMovieVideo(id: Int, apiKey: String) : ResultVideo
}