package com.wiyatmoko.movies.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.data.ResponseDetailMovie
import com.wiyatmoko.movies.data.ResponseMovie
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.data.ResultVideo

class Repository @Inject constructor(
    private val local: LocalDataSource,
    private val remote: RemoteSourceData
) {
    fun getAllMovies(): Flow<PagingData<Result>> {
        return remote.getAllMovies()
    }

    fun getSearch(query: String): LiveData<PagingData<Result>> {
        return remote.getSearchMoviesLiveData(query)
    }

    fun getAllLiveDataMovies(): LiveData<PagingData<Result>> {
        return remote.getAllMoviesLiveData()
    }

    suspend fun getMovieDetailOnline(id: Int, apiKey: String): ResponseDetailMovie {
        return remote.getMovieDetailOnline(id, apiKey)
    }

    suspend fun getMovieDetail(id: Int): Result {
        return local.getSelectedMovies(id = id)
    }

    suspend fun searchMovies(query: String): List<Result> {
        return local.searchMovies(query)
    }

    suspend fun genreMovie(apiKey: String): GenreMovie {
        return remote.getGenre(apiKey)
    }

    suspend fun getMovieGenre(apiKey: String, withGenre: Int) : ResponseMovie{
        return  remote.getMovieGenre(apiKey, withGenre)
    }

    suspend fun getMovieVideo(id: Int, apiKey: String) : ResultVideo{
        return remote.getMovieVideo(id, apiKey)
    }
}