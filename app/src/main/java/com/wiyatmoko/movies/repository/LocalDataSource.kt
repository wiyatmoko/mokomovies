package com.wiyatmoko.movies.repository
import com.wiyatmoko.movies.data.Result
interface LocalDataSource {
    suspend fun getSelectedMovies(id: Int): Result
    suspend fun searchMovies(query:String): List<Result>
}