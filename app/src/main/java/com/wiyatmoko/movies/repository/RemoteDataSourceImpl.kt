package com.wiyatmoko.movies.repository

import androidx.lifecycle.LiveData
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.data.ResponseDetailMovie
import com.wiyatmoko.movies.data.ResponseMovie
import com.wiyatmoko.movies.paging.MovieRemoteMediator
import com.wiyatmoko.movies.data.database.MoviesDatabase
import com.wiyatmoko.movies.data.network.MovieApiService
import com.wiyatmoko.movies.util.Constans
import kotlinx.coroutines.flow.Flow
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.data.ResultVideo
import com.wiyatmoko.movies.paging.PagingSearchSource

@ExperimentalPagingApi
class RemoteDataSourceImpl(
    private val movieApiService: MovieApiService,
    private val moviesDatabase: MoviesDatabase
) : RemoteSourceData {

    private val moviesDao = moviesDatabase.moviesDao()
    override fun getAllMovies(): Flow<PagingData<Result>> {
        val pagingSourceFactory = { moviesDao.readMovies() }
        return Pager(
            config = PagingConfig(
                pageSize = Constans.MAX_ITEM_SIZE,
                enablePlaceholders = false
            ),
            remoteMediator = MovieRemoteMediator(
                movieApiService = movieApiService,
                moviesDatabase = moviesDatabase
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    override fun getAllMoviesLiveData(): LiveData<PagingData<Result>> {
        val pagingSourceFactory = { moviesDao.readMovies() }
        return Pager(
            config = PagingConfig(
                pageSize = Constans.MAX_ITEM_SIZE,
                enablePlaceholders = false

            ),
            remoteMediator = MovieRemoteMediator(
                movieApiService = movieApiService,
                moviesDatabase = moviesDatabase
            ),
            pagingSourceFactory = pagingSourceFactory
        ).liveData
    }

    override fun getSearchMoviesLiveData(query: String): LiveData<PagingData<Result>> {

        return Pager(
            config = PagingConfig(pageSize = 20),
            pagingSourceFactory = {
                PagingSearchSource(
                    movieApiService = movieApiService, query = query,
                    moviesDatabase = moviesDatabase
                )
            }
        ).liveData
    }

    override fun getMovieDetail(id: Int): Result {
        return moviesDao.getSelectedMovies(id = id)
    }

    override suspend fun getMovieDetailOnline(id: Int, apiKey: String): ResponseDetailMovie {
        return movieApiService.getMovieDetail(id,apiKey)
    }

    override suspend fun getGenre(apiKey: String): GenreMovie {
        return movieApiService.getGenre(apiKey)
    }

    override suspend fun getMovieGenre(apiKey: String, withGenres: Int): ResponseMovie {
        return movieApiService.getMovieGenre(apiKey, withGenres)
    }

    override suspend fun getMovieVideo(id: Int, apiKey: String): ResultVideo {
        return movieApiService.getVideo(id, apiKey)
    }

    override fun getSearchMovies(): Flow<PagingData<Result>> {
        TODO("Not yet implemented")
    }





}