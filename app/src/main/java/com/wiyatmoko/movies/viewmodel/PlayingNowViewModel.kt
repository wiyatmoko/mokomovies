package com.wiyatmoko.movies.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.wiyatmoko.movies.data.GenreMovie
import com.wiyatmoko.movies.data.ResponseMovie
import com.wiyatmoko.movies.usecase.UseCase
import com.wiyatmoko.movies.util.Constans
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlayingNowViewModel @Inject constructor(
    private val useCase: UseCase,
    application: Application
) : AndroidViewModel(application) {
    val getAllMoviesLiveData = useCase.getAllMoviesUseCase.getPagingNowPlayingMovieLiveData().cachedIn(viewModelScope)
    var genreMovieData = MutableLiveData<GenreMovie>()
    var _genreDataMovie = MutableLiveData<ResponseMovie>()


    suspend fun getGenre(apiKey: String){
        genreMovieData.value = useCase.genreMovie.getGenre(apiKey)
    }

    suspend fun getMovieGenre(apiKey: String, withGenres: Int){
        _genreDataMovie.value =  useCase.getGenreMovie.getMovieGenre(apiKey, withGenres);
    }

}