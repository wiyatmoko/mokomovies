package com.wiyatmoko.movies.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wiyatmoko.movies.usecase.UseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.wiyatmoko.movies.data.Result

@HiltViewModel
class SearchMovieViewModel @Inject constructor(
    private val useCase: UseCase,
    application: Application
) : AndroidViewModel(application) {
    var searchMovieList: MutableLiveData<List<Result>> = MutableLiveData()
    fun searchMovie(query: String) {
        val launch = viewModelScope.launch(Dispatchers.IO) {
            searchMovieList.postValue(useCase.searchMovieLocalUseCase.searchMoviesLocal(query))
        }
    }
}
