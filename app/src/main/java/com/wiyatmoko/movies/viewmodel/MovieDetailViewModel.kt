package com.wiyatmoko.movies.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.wiyatmoko.movies.data.ResponseDetailMovie
import com.wiyatmoko.movies.usecase.UseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.data.ResultVideo
import com.wiyatmoko.movies.util.Constans

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val useCase: UseCase,
    application: Application
) : AndroidViewModel(application) {
    val movieList: MutableLiveData<Result> = MutableLiveData(null)
    var _movieList = MutableLiveData<ResponseDetailMovie>()
    var youtubeVideo = MutableLiveData<ResultVideo>()

    suspend fun getMovieVideo(id: Int, apiKey: String){
        youtubeVideo.value = useCase.getMovieVideo.getMovieVideo(id, apiKey)
    }

    fun getMovieDetail(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            movieList.postValue(useCase.movieDetailUseCases.getMovieDetail(id = id))
        }
    }

    suspend fun getMovieDetailOnline(id: Int){
           _movieList.value = useCase.movieDetailOnlineUseCases.getMovieDetailOnline(id, Constans.API_KEY)
    }
}