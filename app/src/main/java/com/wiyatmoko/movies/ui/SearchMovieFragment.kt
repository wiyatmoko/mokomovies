package com.wiyatmoko.movies.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.wiyatmoko.movies.R
import com.wiyatmoko.movies.databinding.FragmentSearchMovieBinding
import com.wiyatmoko.movies.ui.adapter.MoviePlayingAdapter
import com.wiyatmoko.movies.util.NetworkListener
import com.wiyatmoko.movies.viewmodel.SearchMovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import com.wiyatmoko.movies.data.Result

@AndroidEntryPoint
class SearchMovieFragment : Fragment(), androidx.appcompat.widget.SearchView.OnQueryTextListener {
    private var _binding: FragmentSearchMovieBinding? = null
    private val binding get() = _binding!!
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: MoviePlayingAdapter
    private lateinit var networkListener: NetworkListener
    private val searchViewModel: SearchMovieViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSearchMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        observerSearch()
    }

    private fun setupRecycleView() {
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.searchRecyclerview.layoutManager = linearLayoutManager
        binding.searchRecyclerview.adapter = adapter
        binding.searchRecyclerview.setHasFixedSize(true)

    }

    private fun observerSearch() {
        searchViewModel.searchMovieList.observe(viewLifecycleOwner) {
            postValue(it)
        }
    }

    private fun postValue(result: List<Result>) {
        if (result.isNotEmpty()) {
            adapter = MoviePlayingAdapter(requireContext(), result)
            setupRecycleView()
            adapter.setOnclicklistener(object : MoviePlayingAdapter.OnClickListener {
                override fun onClick(position: Int, model: Result) {
                    gotoDetailMovie(model.id)
                }

            })

        } else {
            Toast.makeText(requireContext(), "Data tidak ditemukan silahkan cari sesuai yang di list data", Toast.LENGTH_LONG).show()
        }

    }


    private fun gotoDetailMovie(data: Int) {
        val action: NavDirections =
            SearchMovieFragmentDirections.actionSearchMovieFragmentToDetailMovieFragment(data)
        Navigation.findNavController(binding.searchRecyclerview).navigate(action)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.movies_search, menu)

        val search = menu.findItem(R.id.menu_search)
        val searchView = search.actionView as? androidx.appcompat.widget.SearchView
        searchView?.isSubmitButtonEnabled = true
        searchView?.setOnQueryTextListener(this)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (query?.isNotEmpty() == true) {
            searchViewModel.searchMovie(query = query)
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }
}


