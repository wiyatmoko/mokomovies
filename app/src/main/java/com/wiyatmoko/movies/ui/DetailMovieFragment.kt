package com.wiyatmoko.movies.ui

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import android.widget.MediaController
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.wiyatmoko.movies.R
import com.wiyatmoko.movies.data.ResponseDetailMovie
import com.wiyatmoko.movies.databinding.FragmentDetailMovieBinding
import com.wiyatmoko.movies.viewmodel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import com.wiyatmoko.movies.data.Result
import kotlinx.coroutines.launch


@AndroidEntryPoint
class DetailMovieFragment : Fragment() {

    private val viewModel: MovieDetailViewModel by viewModels()
    private var movieId = 0
    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!
    private val args : DetailMovieFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {



        arguments?.let {
            movieId = DetailMovieFragmentArgs.fromBundle(it).id
        }
        if (movieId != 0) {
            viewModel.getMovieDetail(movieId)
            observeViewModel()
        }

        if(args.id != 0){
            lifecycleScope.launch{
                viewModel.getMovieDetailOnline(args.id)
                observeViewModel()
            }
        }

        binding.btnYoutube.setOnClickListener {
            val action: NavDirections =
                DetailMovieFragmentDirections.actionDetailMovieFragmentToYoutubeVideoFragment(args.id)
            Navigation.findNavController(binding.btnYoutube).navigate(action)
        }

    }

    private fun observeViewModel() {
        viewModel.movieList.observe(viewLifecycleOwner) {
            postValue(it)
        }

        viewModel._movieList.observe(viewLifecycleOwner){
            postValueOnline(it)
        }
    }

    private fun postValueOnline(it: ResponseDetailMovie?) {
        if (it != null) {
            binding.title.text = it.title
            binding.ratingValue.text = it.popularity.toString()
            binding.summary.text = it.overview
            val imageData = it.backdrop_path
            val glideOpt =
                RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).fitCenter()
                    .placeholder(
                        R.drawable.ic_image_blank
                    )
            Glide.with(requireContext())
                .load("https://image.tmdb.org/t/p/w300$imageData")
                .apply(glideOpt)
                .thumbnail(0.1f)
                .into(binding.mainImageview)
        }
    }

    private fun postValue(it: Result?) {
        if (it != null) {
            binding.title.text = it.title
            binding.ratingValue.text = it.popularity.toString()
            binding.summary.text = it.overview
            val imageData = it.backdrop_path
            val glideOpt =
                RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).fitCenter()
                    .placeholder(
                        R.drawable.ic_image_blank
                    )
            Glide.with(requireContext())
                .load("https://image.tmdb.org/t/p/w300$imageData")
                .apply(glideOpt)
                .thumbnail(0.1f)
                .into(binding.mainImageview)
        }
    }
}