package com.wiyatmoko.movies.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.databinding.FragmentListMovieGenreBinding
import com.wiyatmoko.movies.ui.adapter.MoviePlayingAdapter
import com.wiyatmoko.movies.util.Constans
import com.wiyatmoko.movies.viewmodel.PlayingNowViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class FragmentListMovieGenre : Fragment() {
    private val viewModel: PlayingNowViewModel by viewModels()
    private var _binding: FragmentListMovieGenreBinding? = null
    private lateinit var adapter: MoviePlayingAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private val binding get() = _binding!!

    private val args : FragmentListMovieGenreArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListMovieGenreBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findNavController().previousBackStackEntry?.destination?.id
        observeViewModel()
        lifecycleScope.launch {
            viewModel.getMovieGenre(Constans.API_KEY, args.id)
        }

    }

    private fun observeViewModel() {
        viewModel._genreDataMovie.observe(viewLifecycleOwner){
            postValue(it.results)
        }
    }

    private fun postValue(results: List<Result>) {
        if (results.isNotEmpty()) {
            adapter = MoviePlayingAdapter(requireContext(), results)
            setupRecycleView()
            adapter.setOnclicklistener(object : MoviePlayingAdapter.OnClickListener {
                override fun onClick(position: Int, model: Result) {
                    val action: NavDirections =
                        FragmentListMovieGenreDirections.actionFragmentListMovieGenreToDetailMovieFragment(model.id)
                    Navigation.findNavController(binding.genreListMovieRecyclerview).navigate(action)
                }

            })

        } else {
            Toast.makeText(requireContext(), "Data tidak ditemukan silahkan cari sesuai yang di list data", Toast.LENGTH_LONG).show()
        }
    }

    private fun setupRecycleView() {
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.genreListMovieRecyclerview.layoutManager = linearLayoutManager
        binding.genreListMovieRecyclerview.adapter = adapter
        binding.genreListMovieRecyclerview.setHasFixedSize(true)

    }
}