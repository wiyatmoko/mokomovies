package com.wiyatmoko.movies.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.wiyatmoko.movies.data.ResultVideo
import com.wiyatmoko.movies.data.ResultYoutubeVideo
import com.wiyatmoko.movies.databinding.FragmentYoutubeVideoBinding
import com.wiyatmoko.movies.util.Constans
import com.wiyatmoko.movies.viewmodel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class YoutubeVideoFragment : Fragment() {
    private var _binding: FragmentYoutubeVideoBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MovieDetailViewModel by viewModels()

    var idVideo : String? = null
    private val args : YoutubeVideoFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentYoutubeVideoBinding.inflate(inflater, container, false)
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        if(args.id != 0){
            lifecycleScope.launch{
                viewModel.getMovieDetailOnline(args.id)
                viewModel.getMovieVideo(args.id, Constans.API_KEY)
                observeViewModel()

            }
        }


    }

    private fun observeViewModel() {
        viewModel.youtubeVideo.observe(viewLifecycleOwner) {
            postValue(it)
        }
    }

    private fun postValue(it: ResultVideo?) {
        if(it?.results != null){
            val resultYoutubeVideo :ResultYoutubeVideo = it.results.get(0)
            idVideo = resultYoutubeVideo.key
            binding.youtubeWebView.webChromeClient = WebChromeClient()
            binding.youtubeWebView.loadUrl("https://www.youtube.com/watch?v="+idVideo)
        }
    }


}