package com.wiyatmoko.movies.ui

import android.R
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wiyatmoko.movies.data.Genre
import com.wiyatmoko.movies.databinding.FragmentListMovieBinding
import com.wiyatmoko.movies.ui.adapter.MovieListAdapter
import com.wiyatmoko.movies.viewmodel.PlayingNowViewModel
import com.wiyatmoko.movies.viewmodel.SearchMovieViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.util.Constans
import com.wiyatmoko.movies.util.NetworkListener
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class FragmentListMovie : Fragment() {

    private val adapter: MovieListAdapter by lazy { MovieListAdapter() }
    private val viewModel: PlayingNowViewModel by viewModels()
    private val searchViewModel: SearchMovieViewModel by viewModels()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var networkListener: NetworkListener
    private var _binding: FragmentListMovieBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        initAdapter()
        setupRecycleView()
        binding.toSearch.setOnClickListener {
            gotoSearchMovie()

        }
        lifecycleScope.launch {
            viewModel.getGenre(Constans.API_KEY)
        }
    }

    private fun postValue(genres: List<Genre>) {
        var genreName  = mutableListOf<String>()
        genreName.add("Select Genre Movie")
        for (value in genres) {
            genreName.add(value.name)
        }

        val adapter = context?.let {
            ArrayAdapter(
                it,
                android.R.layout.simple_spinner_item, genreName)
        }
        binding.spinerGenre.adapter = adapter

        binding.spinerGenre.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View, position: Int, id: Long) {

                for (value in genres) {
                    if(value.name.equals(genreName[position])){
//                        Toast.makeText(context, value.id.toString(), Toast.LENGTH_SHORT).show()
                            gotoMovieGenre(value.id)
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
    }

    private fun setupRecycleView() {
        val recyclerView: RecyclerView = binding.recyclerViewHome
        linearLayoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = adapter

        adapter.setOnclicklistener(object : MovieListAdapter.OnClickListener {
            override fun onClick(data: Result?) {
                if (data != null) {
                    gotoDetailMovie(data)
                }
            }
        })
    }

    private fun observeViewModel() {
        viewModel.getAllMoviesLiveData.observe(viewLifecycleOwner) {
            adapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        viewModel.genreMovieData.observe(viewLifecycleOwner){
            postValue(it.genres)
        }
    }


    private fun initAdapter() {
        adapter.addLoadStateListener { loadState ->
            binding.recyclerViewHome.isVisible =
                loadState.refresh is LoadState.NotLoading && adapter.itemCount == 0
            binding.recyclerViewHome.isVisible =
                loadState.mediator?.refresh is LoadState.NotLoading || loadState.source.refresh is LoadState.NotLoading
            binding.progressView.isVisible = loadState.mediator?.refresh is LoadState.Loading
            binding.progressView.isVisible =
                loadState.refresh is LoadState.Error && adapter.itemCount == 0
        }
    }

    fun gotoMovieGenre(idGenre: Int){
        val action: NavDirections =
            FragmentListMovieDirections.actionListMovieFragmentToFragmentListMovieGenre(idGenre)
        Navigation.findNavController(binding.recyclerViewHome).navigate(action)
    }

    fun gotoDetailMovie(data: Result) {
        val action: NavDirections =
           FragmentListMovieDirections.actionListMovieFragmentToDetailMovieFragment(data.id)
        Navigation.findNavController(binding.recyclerViewHome).navigate(action)
    }

    private fun gotoSearchMovie() {
        val action: NavDirections =
            FragmentListMovieDirections.actionListMovieFragmentToSearchMovieFragment()
        Navigation.findNavController(binding.toSearch).navigate(action)

    }
}