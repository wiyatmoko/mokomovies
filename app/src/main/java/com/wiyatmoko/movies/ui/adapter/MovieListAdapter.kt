package com.wiyatmoko.movies.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.wiyatmoko.movies.R
import com.wiyatmoko.movies.databinding.LayoutItemMovieBinding
import com.wiyatmoko.movies.data.Result


class MovieListAdapter :
    PagingDataAdapter<Result, MovieListAdapter.MovieListViewHolder>(MovieDiffCallBack()) {
    private var onClickListener: OnClickListener? = null

    interface OnClickListener {
        fun onClick(data: Result?)

    }


    fun setOnclicklistener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    override fun onBindViewHolder(holder: MovieListViewHolder, position: Int) {
        val data = getItem(position)
        data?.let { holder.bind(it) }
        holder.itemView.setOnClickListener {
            try {
                if (onClickListener != null) onClickListener!!.onClick(data)
            }catch (e:Exception) {
                Log.d("clickable", "error")
            }
        }
    }


    class MovieListViewHolder(
        private val binding: LayoutItemMovieBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(result: Result) {
            val imageData = result.backdrop_path
            val glideOpt =
                RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).fitCenter()
                    .placeholder(R.drawable.ic_image_blank)
            Glide.with(binding.movieImageView.context)
                .load("https://image.tmdb.org/t/p/w500$imageData")
                .apply(glideOpt)
                .thumbnail(0.1f)
                .into(binding.movieImageView)

            binding.titleTextView.text = result.title
            binding.descriptionTextView.text = result.overview


        }
    }

    class MovieDiffCallBack : DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean {
            return oldItem == newItem
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListViewHolder {
        return MovieListViewHolder(
            LayoutItemMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }


}