package com.wiyatmoko.movies.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.wiyatmoko.movies.data.RemoteKeys
import com.wiyatmoko.movies.data.database.MoviesDatabase
import com.wiyatmoko.movies.data.network.MovieApiService
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.util.Constans

@ExperimentalPagingApi
class MovieRemoteMediator(
    private val movieApiService: MovieApiService,
    private val moviesDatabase: MoviesDatabase
) : RemoteMediator<Int, Result>() {
    override suspend fun load(loadType: LoadType, state: PagingState<Int, Result>): MediatorResult {
        val moviesDao = moviesDatabase.moviesDao()
        val moviesDaoKeys = moviesDatabase.remoteKeysDao()
        return try {
            val page = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: 1
                }
                LoadType.PREPEND -> {
                    val remoteKeys = getRemoteKeyForFirstItem(state)
                    if (remoteKeys?.prevKey == null) {
                        return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                    }
                    remoteKeys.prevKey
                }
                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                    if (remoteKeys?.nextKey  == null) {
                        return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                    }
                    remoteKeys.nextKey
                }

            }

            val movieResponse = movieApiService.getMoviePlaying(page, Constans.API_KEY)
            val repos = movieResponse.results
            val endOfPaginationReached = repos.isEmpty()
            if (movieResponse.results.isNotEmpty()) {
                moviesDatabase.withTransaction {
                    if (loadType == LoadType.REFRESH) {
                        moviesDao.clearAll()
                        moviesDaoKeys.clearRemoteKeys()
                    }
                    val prevKey = if (page == movieResponse.page) null else page - 1
                    val nextKey = if (endOfPaginationReached) null else page + 1
                    val keys = movieResponse.results.map {
                        RemoteKeys(repoId = it.id, prevKey = prevKey, nextKey = nextKey)
                    }
                    moviesDao.insertAllMovies(moviesEntity = movieResponse.results)
                    moviesDaoKeys.insertAll(keys)
                }
            }

            MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (e: Exception) {
            return MediatorResult.Error(e)
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, Result>
    ): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                moviesDatabase.remoteKeysDao().remoteKeysRepoId(repoId)
            }
        }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, Result>): RemoteKeys? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { repo ->
                // Get the remote keys of the first items retrieved
                moviesDatabase.remoteKeysDao().remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, Result>): RemoteKeys? {
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { repo ->
                // Get the remote keys of the last item retrieved
                moviesDatabase.remoteKeysDao().remoteKeysRepoId(repo.id)
            }
    }
}