package com.wiyatmoko.movies.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.wiyatmoko.movies.data.network.MovieApiService
import javax.inject.Inject
import com.wiyatmoko.movies.data.Result
import com.wiyatmoko.movies.data.database.MoviesDatabase

class PagingSearchSource @Inject constructor(
    private val movieApiService: MovieApiService,
    private val moviesDatabase: MoviesDatabase,
    private val query: String
) : PagingSource<Int, Result>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        val moviesDao = moviesDatabase.moviesDao()
        return try {
            val pageNumber = params.key ?: 1
            val prevKey = if (pageNumber > 0) pageNumber - 1 else null
            val apiResponse =
                movieApiService.getMovieSearch(query = query, "6bc90b57c5647cd87ee44270e4b52998",pageNumber)
            val movieList = apiResponse.results
            val nextKey = if (movieList.isNotEmpty()) pageNumber + 1 else null
            if(movieList.isNotEmpty()){
                LoadResult.Page(
                    data = movieList,
                    prevKey = prevKey,
                    nextKey = nextKey
                )
            } else {
                val listMovie = moviesDao.searchMovies(query)
                LoadResult.Page(
                    data = listMovie,
                    prevKey = prevKey,
                    nextKey = nextKey
                )
            }


        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {
        return state.anchorPosition
    }
}