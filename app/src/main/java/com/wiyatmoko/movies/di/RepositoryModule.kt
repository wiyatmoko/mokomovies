package com.wiyatmoko.movies.di

import com.wiyatmoko.movies.repository.Repository
import com.wiyatmoko.movies.usecase.GetAllMoviesUseCase
import com.wiyatmoko.movies.usecase.GetGenreMovie
import com.wiyatmoko.movies.usecase.GetMovieGenres
import com.wiyatmoko.movies.usecase.GetMovieVideo
import com.wiyatmoko.movies.usecase.MovieDetailOnlineUseCases
import com.wiyatmoko.movies.usecase.MovieDetailUseCases
import com.wiyatmoko.movies.usecase.SearchMovieLocalUseCase
import com.wiyatmoko.movies.usecase.SearchMovieUseCase
import com.wiyatmoko.movies.usecase.UseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun provideUseCases(repository: Repository) : UseCase {
        return UseCase(
            getAllMoviesUseCase = GetAllMoviesUseCase(repository),
            searchMovieUseCase = SearchMovieUseCase(repository),
            movieDetailUseCases = MovieDetailUseCases(repository),
            movieDetailOnlineUseCases = MovieDetailOnlineUseCases(repository),
            searchMovieLocalUseCase = SearchMovieLocalUseCase(repository),
            genreMovie = GetGenreMovie(repository),
            getGenreMovie = GetMovieGenres(repository),
            getMovieVideo = GetMovieVideo(repository)
        )
    }
}