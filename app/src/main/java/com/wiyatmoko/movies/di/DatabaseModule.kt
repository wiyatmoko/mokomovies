package com.wiyatmoko.movies.di

import android.content.Context
import androidx.room.Room
import com.wiyatmoko.movies.data.database.MoviesDao
import com.wiyatmoko.movies.data.database.MoviesDatabase
import com.wiyatmoko.movies.repository.LocalDataSource
import com.wiyatmoko.movies.repository.LocalDataSourceImpl
import com.wiyatmoko.movies.util.Constans.Companion.DATABASE_NAME

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ) = Room.databaseBuilder(
        context,
        MoviesDatabase::class.java,
        DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun provideDao(database: MoviesDatabase) = database.moviesDao()


    @Singleton
    @Provides
    fun provideLocalDataSource(
        database: MoviesDatabase
    ): LocalDataSource {
        return LocalDataSourceImpl(
            database = database
        )
    }
}